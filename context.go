package main

type Context struct {
	Monitor  chan *Task
	Settings Config
}

func NewContext(config Config) *Context {
	return &Context{Monitor: make(chan *Task), Settings: config}
}

func (c *Context) PushTask(t *Task) {
	timeout := c.Settings.BaseTimeoutSec
	count := 0

	if t.AlarmCount != 0 {
		timeout = c.Settings.ExtendedTimeoutSec
		count = t.AlarmCount - 1
	}

	c.Monitor <- &Task{
		Url:        t.Url,
		Timeout:    timeout,
		AlarmCount: count,
	}
}

func (c *Context) PushAlarmTask(t *Task) {
	c.Monitor <- &Task{
		Url:        t.Url,
		Timeout:    c.Settings.ExtendedTimeoutSec,
		AlarmCount: c.Settings.ExtendedTimeoutCount,
	}
}

func (c *Context) AddInitTasks() {
	go func() {
		for _, u := range c.Settings.Urls {
			c.Monitor <- &Task{Url: u, Timeout: 0, AlarmCount: 0}
		}
	}()
}
