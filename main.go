package main

import (
	"encoding/json"
	"flag"
	"log"
	"os"
	"os/signal"
)

type Config struct {
	Urls                 []string `json:"urls"`
	BaseTimeoutSec       int      `json:"base_timeout_sec"`
	ExtendedTimeoutSec   int      `json:"extended_timeout_sec"`
	ExtendedTimeoutCount int      `json:"extended_timeout_count"`
}

func main() {
	log.Println("Service start")
	defer log.Println("Service stop")

	settings, err := LoadConfig()
	if err != nil {
		log.Fatalln("Error with config file: ", err.Error())
	}

	context := NewContext(settings)
	context.AddInitTasks()

	// Catch Ctrl+C
	osEvents := make(chan os.Signal, 1)
	signal.Notify(osEvents, os.Interrupt)

	for {
		select {
		case t := <-context.Monitor:
			go ProcessTask(context, t)
		case <-osEvents:
			return
		}
	}
}

func LoadConfig() (settings Config, err error) {
	path := flag.String("c", "config.json", "Path to configuration file (*.json)")
	flag.Parse()

	configFile, err := os.Open(*path)
	if err != nil {
		return
	}

	jsonParser := json.NewDecoder(configFile)
	if err = jsonParser.Decode(&settings); err != nil {
		return
	}

	return
}
