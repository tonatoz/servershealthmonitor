package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestBadServer(t *testing.T) {
	badServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusServiceUnavailable)
	}))
	defer badServer.Close()

	err := CheckHealth(badServer.URL)

	if err.Error() != fmt.Sprintf("Server %s send bad status code - 503", badServer.URL) {
		t.Errorf("Expected 503 status code, actualy - %s", err.Error())
	}
}

func TestGoodServer(t *testing.T) {
	goodServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}))
	defer goodServer.Close()

	err := CheckHealth(goodServer.URL)

	if err != nil {
		t.Errorf("Expected continiue work, actualy have error - %s", err.Error())
	}
}

func TestAlarmTaskFlow(t *testing.T) {
	context := NewContext(Config{
		BaseTimeoutSec:       5,
		ExtendedTimeoutSec:   1,
		ExtendedTimeoutCount: 3,
	})

	go context.PushAlarmTask(&Task{Url: "http://yandex.ru/", AlarmCount: 0, Timeout: 5})

	testTimeout := func(timeout int) {
		if timeout != 1 {
			t.Errorf("Expected timeout 1, actualy - %d", timeout)
		}
	}

	testCount := func(expected int, actual int) {
		if expected != actual {
			t.Errorf("Expected count %d, actualy - %d", expected, actual)
		}
	}

	t1 := <-context.Monitor
	testTimeout(t1.Timeout)
	testCount(t1.AlarmCount, 3)
	go context.PushTask(t1)

	t2 := <-context.Monitor
	testTimeout(t2.Timeout)
	testCount(t2.AlarmCount, 2)
	go context.PushTask(t2)

	t3 := <-context.Monitor
	testTimeout(t3.Timeout)
	testCount(t3.AlarmCount, 1)
	go context.PushTask(t3)

	t4 := <-context.Monitor
	testCount(t4.AlarmCount, 0)
}
