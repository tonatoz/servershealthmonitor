package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"
)

type Task struct {
	Url        string
	Timeout    int
	AlarmCount int
}

func ProcessTask(c *Context, task *Task) {
	time.Sleep(time.Duration(task.Timeout) * time.Second)

	if err := CheckHealth(task.Url); err != nil {
		log.Printf("[Alarm] - %s", err.Error())
		c.PushAlarmTask(task)
		return
	}

	c.PushTask(task)
	return
}

func CheckHealth(url string) error {
	resp, err := http.Head(url)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return errors.New(fmt.Sprintf("Server %s send bad status code - %d", url, resp.StatusCode))
	}

	return nil
}
