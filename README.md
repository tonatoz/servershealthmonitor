# ServerHealthMonitor

Test tool for servers uptime monitoring. 

## Installation

1. Install Go 1.6
2. `git clone git@bitbucket.org:tonatoz/servershealthmonitor.git`
3. `cd servershealthmonitor`
4. Build `go build -o monitor`
5. Run `./monitor` 

## Usage

Tool have configuration file. 
By default it take config.json in current directory.
You can set your own configuration file using parameter -c 

Example:

`./monitor -c /tmp/new_config.json`

Configuration file have a very simple structure:

    {
       "urls": [
         "http://yandex.ru/",
         "https://google.com/"
         ],   
       "base_timeout_sec": 5,   
       "extended_timeout_sec": 2,   
       "extended_timeout_count": 5   
    }

* __urls__ contains list of servers to check.
* __base_timeout_sec__ is time in seconds to check url
* __extended_timeout_sec__ is time in seconds to check url, when previous check failed
* __extended_timeout_count__ set how much times tool will 
use _extended_timeout_sec_ to url and after will use _base_timeout_sec_
